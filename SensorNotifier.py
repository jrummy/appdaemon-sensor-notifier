from enum import Enum    
from datetime import datetime, timedelta
import uuid
import appdaemon.plugins.hass.hassapi as hass

class LogLevels(Enum):
    SN_ERR = 3
    SN_WARNING = 4
    SN_NOTICE = 5
    SN_INFO = 6
    SN_DEBUG = 7 

    def __int__(self):
        return self.value


class NotifierSettingCondition(object):
    
    def __init__(self, args):
        self._label = None

        for key, value in args.items():
            if key == "expression_label":
                self._label = value
            else:
                if not self._label:
                    self._label = key
                self._entity = key
                self._target_state = value


    @property
    def label(self):
        return self._label


    @property
    def entity(self):
        return self._entity


    @property
    def target_state(self):
        return self._target_state



class NotifierSetting(object):
    CONDITION_LOGIC_SIMPLE = 1
    CONDITION_LOGIC_EXPRESSION = 2
    
    def __init__(self, args):
        self._condition_logic = self.CONDITION_LOGIC_SIMPLE

        try:
            self._setting = args["setting"]
        except KeyError:
            raise ValueError("notifier input setting entry requires 'setting'")

        try:
            self._temporary = args["temporary"]
        except KeyError:
            self._temporary = False

        self._conditions = {}
        self._condition_match_all = True
        try:
            self._condition_match_all = args["condition_match_all"]
        except KeyError:
            self._condition_match_all = True

        try:
            for condition in args["conditions"]:
                condition_obj = NotifierSettingCondition(condition)
                if condition_obj.label:
                    self._conditions[condition_obj.label] = condition_obj
                elif condition_obj.entity:
                    self._conditions[condition_obj.entity] = condition_obj
        except KeyError:
            pass
        except Exception as e:
            raise ValueError("Error loading setting condition from configuration: %s" % str(e))

    
    def __eq__(self, comp):
        if not comp: 
            return False

        return (str(comp) == self._setting)


    @property
    def setting(self):
        return self._setting


    @property
    def temporary(self):
        return self._temporary


    @property
    def conditions(self):
        return self._conditions


    @property
    def condition_match_all(self):
        return self._condition_match_all


    def condition_logic_simple(self):
        return self._condition_logic == self.CONDITION_LOGIC_SIMPLE

    def condition_logic_expression(self):
        return self._condition_logic == self.CONDITION_LOGIC_EXPRESSION

class Notifier(object):
    _last_setting = None
    _notify_entity = None
    _sensor_value = None
    _notify_data = None
    _input = None
    _settings = None
    
    def __init__(self, args):

        try:
            self._notify_entity = args["notifier"]
            self._sensor_value = args["sensor_value"]
        except KeyError:
            raise ValueError("notifier requires a notifier entity and a sensor_value")

        try:
            self._notify_data = args["data"]
        except KeyError:
            pass

        try:
            self._input = args["input"]
            self._settings = {}
            for input_setting in args["input_settings"]:
                try:
                    setting_obj = NotifierSetting(input_setting)
                    self._settings[setting_obj.setting] = setting_obj
                except Exception as e:
                    raise AttributeError(str(e))
        except KeyError:
            pass


    def __str__(self):
        return ("notify_entity: %s | sensor_value: %s | notify_data: %s | input: %s | setting-count: %d" % 
                    (self.notify_entity, self.sensor_value, self.notify_data, self.input, len(self.settings)))

    @property
    def notify_entity(self):
        return self._notify_entity


    @property
    def sensor_value(self):
        return self._sensor_value


    @property
    def notify_data(self):
        return self._notify_data


    @property
    def input(self):
        return self._input


    @property
    def settings(self):
        return self._settings


    @property
    def temp_settings(self):
        return self._temp_settings


    @property
    def last_setting(self):
        return self._last_setting

    @last_setting.setter
    def last_setting(self, value):
        self._last_setting = value



class SensorNotifier(hass.Hass):
    conf = None
    
    def initialize(self):
        self.conf = {}

        try: 
            self.conf["log_control"] = self.args["log_control"]
        except KeyError: 
            self.conf["log_control"] = None

        self.set_log_level()
        if self.conf["log_control"] != None:
            self.listen_state(self.log_control_change_callback, self.conf["log_control"])

        self.load_sensor_config(self.args)
        for sensor, notifiers in self.conf["notifiers"].items():
            self.sn_log(LogLevels.SN_DEBUG, "Config loaded sensor %s" % sensor)
            for notifier in notifiers:
                self.sn_log(LogLevels.SN_DEBUG, "   |-> notifier: %s" % str(notifier))
        
        self.register_sensor_listeners()
        self.register_input_listeners()
        # TODO: Schedule a reset of temporary settings


    def sn_log(self, level, msg):
        try: 
            int(level)
        except (TypeError, ValueError) as e:
            self.log("WARNING: Invalid usage of sn_log - non-integer level parameter: %s.  Using SN_WARNING" % level)
            level = LogLevels.SN_WARNING

        try:
            log_level = self.conf["log_level"]
            if log_level == None:
                log_level = int(LogLevels.SN_INFO)
            else:
                log_level = int(self.conf["log_level"])
        except KeyError:
            log_level = int(LogLevels.SN_INFO)
        if int(level) > log_level:
            return

        if level == LogLevels.SN_ERR:
            lvl = "ERROR:"
        elif level == LogLevels.SN_WARNING:
            lvl = "WARNING:"
        elif level == LogLevels.SN_NOTICE:
            lvl = "NOTICE:"
        elif level == LogLevels.SN_INFO:
            lvl = "INFO:"
        elif level == LogLevels.SN_DEBUG:
            lvl = "DEBUG:"
        else: 
            self.log("WARNING: Invalid usage of sn_log - level parameter out of range: %d. Using.SN_WARNING" % level)
            level = LogLevels.SN_WARNING
            lvl = "WARNING:"

        self.log("%s %s" % (lvl, msg))


    def set_log_level(self):
        if self.conf["log_control"] == None:
            self.sn_log(LogLevels.SN_NOTICE, "Log level updated to None - defaulting to INFO")
            return

        value = self.get_state(self.conf["log_control"])
        if value == "ERROR":
            self.conf["log_level"] = LogLevels.SN_ERR
        elif value == "WARNING":
            self.conf["log_level"] = LogLevels.SN_WARNING
        elif value == "NOTICE":
            self.conf["log_level"] = LogLevels.SN_NOTICE
        elif value == "INFO":
            self.conf["log_level"] = LogLevels.SN_INFO
        elif value == "DEBUG":
            self.conf["log_level"] = LogLevels.SN_DEBUG
        else:
            self.sn_log(LogLevels.SN_NOTICE, "Log level update to invalid state: %s - defaulting to INFO" % value)
            return

        self.sn_log(LogLevels.SN_NOTICE, "Log level updated to %s" % value)


    def log_control_change_callback(self, entity, attribute, old, new, kwargs): 
        self.set_log_level()


    def set_config_val(self, input_key, conf_key=None, required=False, num=False, time=False):
        tmpval = None
        try:
            tmpval = self.args[input_key]
        except KeyError: 
            if not required:
                self.sn_log(LogLevels.SN_NOTICE, "%s not found in configuration" % input_key)
                return False
            else:
                raise ValueError("%s not found in configuration" % input_key)
        
        if num == True:
            try:
                int(tmpval)
            except (TypeError, ValueError) as e:
                raise ValueError("%s must be an integer.  Found %s" % (input_key, tmpval))

        if time == True:
            try:
                int(tmpval)
            except (TypeError, ValueError) as e:
                if tmpval != "sunrise" and tmpval != "sunset":
                    raise ValueError("%s must be a time value - either HHMM (24hr) or \"sunrise\" or \"sunset\". Found %s" % (input_key, tmpval))

        if conf_key == None:
            conf_key = input_key
        self.conf[conf_key] = tmpval
        return True


    def load_sensor_config(self, args):
        self.sn_log(LogLevels.SN_DEBUG, "Loading sensors from configuration")
        self.conf["notifiers"] = {}
        self.conf["inputs"] = {}

        try: 
            for sensor in args["sensors"]:
                try: 
                    sensor_entity = sensor["sensor"]
                except KeyError:
                    self.sn_log(LogLevels.SN_WARNING, "Configuration sensor entry missing 'sensor' key")
                    continue

                state = self.get_state(sensor_entity)
                if not state or state == "{}":
                    self.sn_log(LogLevels.SN_WARNING, "Configured sensor %s not found in homeassistant" % sensor_entity)
                    continue

                try:
                    for notifier in sensor["notify"]:
                        notifier_obj = None
                        try:
                            notifier_obj = Notifier(notifier)
                        except Exception as e:
                            self.sn_log(LogLevels.SN_WARNING, "Unable to load sensor notifier for %s: %s" % (sensor_entity, str(e)))
                            continue

                        if notifier_obj: 
                            self.conf["notifiers"].setdefault(sensor_entity, []).append(notifier_obj)
                            if notifier_obj.input:
                                input_settings = self.conf["inputs"].setdefault(notifier_obj.input, {})
                                for setting_value, setting_obj in notifier_obj.settings.items():
                                    input_settings[setting_value] = (notifier_obj, setting_obj)
                except KeyError:
                    self.sn_log(LogLevels.SN_WARNING, "No notifiers found for sensor %s. Nothing to do" % sensor_entity)
                    continue

        except KeyError: 
            self.sn_log(LogLevels.SN_WARNING, "No sensors found in configuration")
            return 


    def register_sensor_listeners(self):
        self.sn_log(LogLevels.SN_DEBUG, "Registering listeners for configured sensors")

        try:
            for sensor, notifiers in self.conf["notifiers"].items():
               self.sn_log(LogLevels.SN_INFO, "Listening for state changes on sensor %s" % sensor)
               canceller = self.listen_state(self.sensor_change_callback, sensor)
        except KeyError:
            pass


    def sensor_change_callback(self, entity, attribute, old, new, kwargs):
        self.sn_log(LogLevels.SN_INFO, "Sensor %s changed from %s to %s" % (entity, old, new))

        try:
            notifiers = self.conf["notifiers"][entity]
        except KeyError: 
            self.sn_log(LogLevels.SN_WARNING, "Sensor %s change detected but sensor not found in configuration" % entity)
            return

        if not notifiers or len(notifiers) == 0:
            self.sn_log(LogLevels.SN_WARNING, "Sensor %s change detected but no notifiers found for this sensor" % entity)

        for notifier in notifiers:
            if self.check_notifier_input_conditions_valid(notifier, new):
                self.sn_log(LogLevels.SN_INFO, "Sensor %s notifier valid for new state %s - calling notifier %s" % (entity, new, notifier.notify_entity))
                if notifier.notify_data:
                    self.call_service(notifier.notify_entity, **notifier.notify_data)
                else:
                    self.call_service(notifier.notify_entity)
            else:
                self.sn_log(LogLevels.SN_DEBUG, "Sensor %s notifier %s not valid" % (entity, notifier.notify_entity))


    def register_input_listeners(self):
        self.sn_log(LogLevels.SN_DEBUG, "Registering listeners for input entities")

        try:
            for input_entity, settings in self.conf["inputs"].items():
                self.sn_log(LogLevels.SN_INFO, "Listening for state changes on input %s" % input_entity)
                canceller = self.listen_state(self.input_change_callback, input_entity)
        except KeyError: 
            pass


    def input_change_callback(self, entity, attribute, old, new, kwargs):
        self.sn_log(LogLevels.SN_INFO, "Input %s changed from %s to %s" % (entity, old, new))

        try:
            (new_notifier, new_setting) = self.conf["inputs"][entity][new]
            (old_notifier, old_setting) = self.conf["inputs"][entity][old]
        except KeyError:
            self.sn_log(LogLevels.SN_WARNING, "Input %s change detected but input or setting not found in configuration" % entity)

        if new_notifier != old_notifier:
            self.sn_log(LogLevels.SN_WARNING, "Input %s change detected but old and new value belong to different notifiers")
            return 

        if new_setting.temporary:
            if not old_setting.temporary:
                new_notifier.last_setting = old_setting
                self.sn_log(LogLevels.SN_INFO, "Remembering input %s setting '%s' as last value after temporary setting '%s' expires" % (entity, old, new))
        else:
            new_notifier.last_setting = None


    def check_setting_condition_valid(self, condition_obj):
        self.sn_log(LogLevels.SN_DEBUG, "Checking if condition %s is valid" % condition_obj.label)

        if condition_obj.entity:
            state = self.get_state(condition_obj.entity)
            return (state == condition_obj.target_state)

        return False


    def check_setting_conditions_simple(self, setting_obj):
        self.sn_log(LogLevels.SN_DEBUG, "Checking setting %s conditions using simple logic" % setting_obj.setting)

        if len(setting_obj.conditions) == 0:
            return True

        for label, condition in setting_obj.conditions.items():
            condition_valid = self.check_setting_condition_valid(condition)
            # all conditions need to be valid, and this one isnt valid
            if (not condition_valid) and setting_obj.condition_match_all:
                return False
            # any condition needs ot be valid, and this one is valid
            elif condition_valid and (not setting_obj.condition_match_all):
                return True

        # all conditions need to be valid, and we didnt find any that are not
        if setting_obj.condition_match_all:
            return True
        # any conditions need to be valid, and we didnt find any that are
        if not setting_obj.condition_match_all: 
            return False

    def check_setting_conditions(self, setting_obj):
        if setting_obj.condition_logic_simple():
            return self.check_setting_conditions_simple(setting_obj)
        elif setting_obj.condition_logic_expression():
            # TODO: Not implemented
            return False
        else:
            # Invalid
            return False

    def check_notifier_input_conditions_valid(self, notifier, sensor_value):
        self.sn_log(LogLevels.SN_DEBUG, "Checking notifier %s conditions" % notifier.notify_entity)

        if notifier.sensor_value != sensor_value:
            self.sn_log(LogLevels.SN_DEBUG, "Notifier %s target-value %s not a match to %s" % (notifier.notify_entity, notifier.sensor_value, sensor_value))
            return False

        if not notifier.input:
            self.sn_log(LogLevels.SN_DEBUG, "Notifier %s is valid match - no conditions and state is a match" % notifier.notify_entity)
            return True

        input_state = self.get_state(notifier.input)
        try:
            setting_obj = notifier.settings[input_state]
        except KeyError:
            self.sn_log(LogLevels.SN_NOTICE, "Notifier input %s state %s not found in configuration" % (notifier.input, input_state))
            return False

        # TODO: Something bad is happening with loggin in this path.  Logs dont print anything if the conditions are not valid.  
        setting_valid = self.check_setting_conditions(setting_obj)
        if setting_valid:
            self.sn_log(LogLevels.SN_DEBUG, "Notifier input %s setting %s conditions valid" % (notifier.input, input_state))
            return True
        else:
            self.sn_log(LogLevels.SN_DEBUG, "Notifier input %s setting conditions not valid" % notifier.input)
            return False
